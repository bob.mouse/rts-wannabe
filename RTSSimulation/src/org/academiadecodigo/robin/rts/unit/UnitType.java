package org.academiadecodigo.robin.rts.unit;

public enum UnitType {

    WORKER,
    FOOTMAN,
    KNIGHT,
    ARCHER,
    WIZARD

}
