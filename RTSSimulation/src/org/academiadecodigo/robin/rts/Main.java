package org.academiadecodigo.robin.rts;

import org.academiadecodigo.robin.rts.building.BuildingType;
import org.academiadecodigo.robin.rts.unit.Worker;

public class Main {

    public static void main(String[] args) {

        Worker worker = new Worker();
        Worker worker1 = new Worker();

        var t1 = new Thread(new Runnable() {
            @Override
            public void run() {
                worker.build(BuildingType.STRONGHOLD);
            }
        });

        var t2 = new Thread(new Runnable() {
            @Override
            public void run() {
                worker1.build(BuildingType.STRONGHOLD);
            }
        });

        t1.start();
        t2.start();

    }
}
