package org.academiadecodigo.robin.rts.unit;

import org.academiadecodigo.robin.rts.building.Building;
import org.academiadecodigo.robin.rts.building.BuildingType;
import org.academiadecodigo.robin.rts.factories.BuildingFactory;
import org.academiadecodigo.robin.rts.resources.GoldMine;
import org.academiadecodigo.robin.rts.resources.Tree;

import java.util.Timer;
import java.util.TimerTask;

public class Worker extends Unit {

    private static final int RESOURCE_CAPACITY = 10;
    private int currentAmount;
    private boolean full;

    public Worker(){
        super(70,10);
        full = false;

    }

    public Building build(BuildingType buildingType){
        Building building = BuildingFactory.createBuilding(buildingType);

        //TODO : get a better solution for this shit later
        building.build(1);

        return building;
    }

    public void gatherGold(GoldMine goldMine){

        if (full) {
            return;
        }
        currentAmount = goldMine.collect(RESOURCE_CAPACITY);
        full = true;
    }

    public void gatherWood(Tree tree){

        if (full){
            return;
        }
        currentAmount = tree.collect(RESOURCE_CAPACITY);
        full = true;
    }

    public int deliverGold(){

        full = false;
        return currentAmount;
    }

    public int deliverWood(){

        full = false;
        return currentAmount;
    }



}
