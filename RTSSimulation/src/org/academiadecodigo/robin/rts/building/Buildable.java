package org.academiadecodigo.robin.rts.building;

public interface Buildable {

    void build(int buildingSpeed);
}
