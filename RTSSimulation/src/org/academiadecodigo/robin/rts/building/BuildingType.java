package org.academiadecodigo.robin.rts.building;

public enum BuildingType {

    BARRACKS,
    STRONGHOLD,
    TOWER,

}
