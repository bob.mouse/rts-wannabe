package org.academiadecodigo.robin.rts.factories;

import org.academiadecodigo.robin.rts.building.*;

public class BuildingFactory {

    public static Building createBuilding(BuildingType buildingType){

        Building building = null;

        switch (buildingType){
            case BARRACKS:
                building = new Barracks();
                break;

            case STRONGHOLD:
                building = new StrongHold();
                break;
            case TOWER:
                building = new Tower();
                break;
        }

        return building;

    }
}