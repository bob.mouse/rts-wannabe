package org.academiadecodigo.robin.rts.utilities;

public class Randomizer {

    public static double randomize(double min, double max){
        return (Math.random() * max) + min;

    }
}
