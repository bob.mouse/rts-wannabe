package org.academiadecodigo.robin.rts.resources;

public class GoldMine extends Resource {

    private static final int MAX_GOLD = 20000;

    public GoldMine(){
        currentAmount = MAX_GOLD;
    }

}
