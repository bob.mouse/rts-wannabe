package org.academiadecodigo.robin.rts.damageInterfaces;

public interface Hittable {

    void suffer(int damage);
}
